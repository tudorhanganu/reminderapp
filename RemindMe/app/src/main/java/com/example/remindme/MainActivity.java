package com.example.remindme;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.app.TimePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.opengl.Visibility;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements View.OnClickListener  {

    static ArrayList<PendingIntent> intents = new ArrayList<>();

    private static final String CHANNEL_ID = "0";
    private static final String sTagAlarms = ":alarms";

    Button dateBtn;
    Button timeBtn;
    Button createBtn;

    Spinner spinner;

    TextView timeString;
    TextView dateString;

    TextView eventName;

    LinearLayout intentLayout;

    private AlarmManager alarmMgr;
    private PendingIntent alarmIntent;

    SharedPreferences.Editor editor;
    SharedPreferences sharedPref;


    private int mYear, mMonth, mDay, mHour, mMinute;

    /**
     * Base method to create the app, sets all the variables
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Locale locale = new Locale("en");
        Configuration config = getBaseContext().getResources().getConfiguration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dateBtn = (Button)findViewById(R.id.dateBtn);
        timeBtn = (Button)findViewById(R.id.timeBtn);
        createBtn = (Button)findViewById(R.id.createBtn);

        dateString=(TextView)findViewById(R.id.dateString);
        timeString=(TextView)findViewById(R.id.timeString);
        eventName=(TextView)findViewById(R.id.eventNameField);

        dateBtn.setOnClickListener(this);
        timeBtn.setOnClickListener(this);
        createBtn.setOnClickListener(this);

        intentLayout = (LinearLayout)findViewById(R.id.intentLayout);

        spinner = (Spinner)findViewById(R.id.spinner4);

        sharedPref = this.getPreferences(Context.MODE_PRIVATE);
        editor = sharedPref.edit();

        displayAlarms(this);
    }

    /**
     * On click method, for all buttons in the app, either adding date, time or create event
     *
     * @param v
     */
    @Override
    public void onClick(View v){
        if (v == dateBtn){
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                    new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                            dateString.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                            mYear = year;
                            mMonth = monthOfYear;
                            mDay = dayOfMonth;
                        }
                    }, mYear, mMonth, mDay);
            datePickerDialog.show();
        }
        if (v == timeBtn){
            final Calendar c = Calendar.getInstance();
            mHour = c.get(Calendar.HOUR_OF_DAY);
            mMinute = c.get(Calendar.MINUTE);

            TimePickerDialog timePickerDialog  = new TimePickerDialog(this,
                    new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                            timeString.setText(hourOfDay + ":" + minute);
                            mHour = hourOfDay;
                            mMinute = minute;
                        }
                    }, mHour, mMinute, false);

            timePickerDialog.show();
        }
        if (v == createBtn){

            final PendingIntent pintent = PendingIntent.getBroadcast( this, sharedPref.getAll().size()+1, new Intent(eventName.getText().toString()), 0 );

            Date date = new Date(mYear-1900, mMonth, mDay, mHour, mMinute);
            Date currDate = new Date(System.currentTimeMillis());

            long milli = date.getTime() - System.currentTimeMillis();
            if (milli > 0){
                editor.putString(sharedPref.getAll().size()+1 +"", eventName.getText().toString() + ";" + spinner.getSelectedItem().toString() + ";" + dateString.getText() + " " + timeString.getText());
                editor.commit();

                BroadcastReceiver receiver = new BroadcastReceiver() {
                    @Override public void onReceive(Context context, Intent _ )
                    {
                        createBtn.setBackgroundColor( Color.RED );
                        context.unregisterReceiver( this ); // this == BroadcastReceiver, not Activity

                        NotificationCompat.Builder mBuilder =
                                new NotificationCompat.Builder(context, CHANNEL_ID)
                                        .setSmallIcon(R.drawable.ic_launcher_background)
                                        .setContentTitle("Reminder")
                                        .setContentText(eventName.getText().toString())
                                        .setContentIntent(pintent)
                                        .setOngoing(true)
                                        .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM));

                        Toast.makeText(context, eventName.getText().toString(), Toast.LENGTH_SHORT).show();

                        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                        notificationManager.notify(0, mBuilder.build());
                    }
                };

                intents.add(pintent);
                displayAlarms(this);

                this.registerReceiver( receiver, new IntentFilter(eventName.getText().toString()) );

                AlarmManager manager = (AlarmManager)(this.getSystemService( Context.ALARM_SERVICE ));

                manager.set( AlarmManager.RTC, currDate.getTime() + milli, pintent );
            }
            else if (eventName.getText().toString().equals("") || eventName.getText().toString() == null){
                AlertDialog alertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("Error")
                        .setMessage("Please enter a name")
                        .show();
            }
            else {
                AlertDialog alertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("Error")
                        .setMessage("Date and time error, cannot be behind in time")
                        .show();
            }

        }

    }

    /**
     * Method to cancel an alarm, once an intent was deleted
     *
     * @param context
     * @param name
     * @param notificationId
     */
    public void cancelAlarm(Context context, String name, int notificationId) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        System.out.println(name + " FROM CANCEL ALARM");
        // double check for errors later, if we need the FLAG_CANCEL_CURRENT or not
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, notificationId, new Intent(name), PendingIntent.FLAG_CANCEL_CURRENT);
        alarmManager.cancel(pendingIntent);
        pendingIntent.cancel();

        removeAlarmId(context, notificationId);
    }

    /**
     * Method linked with cancel alarm, they work together to remove alarms from the phone
     *
     * @param context
     * @param id
     */
    private void removeAlarmId(Context context, int id) {
        List<Integer> idsAlarms = getAlarmIds(context);

        int index = -1;

        for (int i = 0; i < idsAlarms.size(); i++) {
            if (idsAlarms.get(i) == id){
                idsAlarms.remove(i);
                index = i;
                break;
            }
        }

        saveIdsInPreferences(context, id);
    }

    /**
     * Retrieve all alarm ids
     *
     * @param context
     * @return
     */
    private static List<Integer> getAlarmIds(Context context) {
        List<Integer> ids = new ArrayList<>();
        try {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
            JSONArray jsonArray2 = new JSONArray(prefs.getString(context.getPackageName() + sTagAlarms, "[]"));

            for (int i = 0; i < jsonArray2.length(); i++) {
                ids.add(jsonArray2.getInt(i));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return ids;
    }

    /**
     * Remove an element from stored Preferences
     *
     * @param context
     * @param index
     */
    private void saveIdsInPreferences(Context context, int index) {

        sharedPref.edit().remove(index+"").commit();

        editor.apply();
    }

    /**
     * Method to display all the stored alarms in the app.
     *
     * @param context
     */
    private void displayAlarms(final Context context){
        intentLayout.removeAllViews();

        Map<String, ?> events = sharedPref.getAll();

        Iterator it = events.entrySet().iterator();

        while (it.hasNext()){
            final Map.Entry pair = (Map.Entry)it.next();
            final String[] info = pair.getValue().toString().split(";");

            final int id = Integer.parseInt(pair.getKey().toString());

            LinearLayout newLayout = new LinearLayout(context);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

            Button edit = new Button(context);
            edit.setText(getString(R.string.editBtn));
            edit.setLayoutParams(params);
            edit.setTextColor(Color.BLACK);
            edit.setVisibility(View.VISIBLE);
            edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v){
                    TextView toDelete = (TextView)findViewById(id);

                    cancelAlarm(context, toDelete.getText().toString(), id);

                    toDelete.setText(eventName.getText() + "\n" + spinner.getSelectedItem().toString() + " " + dateString.getText() + " " + timeString.getText());

                    sharedPref.getString(Integer.toString(id), null);
                    editor.remove(Integer.toString(id));
                    editor.putString(Integer.toString(id), eventName.getText().toString() + ";" + spinner.getSelectedItem().toString() + ";" + dateString.getText() + " " + timeString.getText());

                    editor.commit();

                    final PendingIntent pintent = PendingIntent.getBroadcast( context, id, new Intent(eventName.getText().toString()), 0 );
                    System.out.println(eventName.getText().toString());

                    Date date = new Date(mYear-1900, mMonth, mDay, mHour, mMinute);
                    Date currDate = new Date(System.currentTimeMillis());

                    long milli = date.getTime() - System.currentTimeMillis();

                    BroadcastReceiver receiver = new BroadcastReceiver() {
                        @Override
                        public void onReceive(Context context, Intent _) {
                            createBtn.setBackgroundColor(Color.RED);
                            context.unregisterReceiver(this); // this == BroadcastReceiver, not Activity

                            NotificationCompat.Builder mBuilder =
                                    new NotificationCompat.Builder(context, CHANNEL_ID)
                                            .setSmallIcon(R.drawable.ic_launcher_background)
                                            .setContentTitle("Reminder")
                                            .setContentText(eventName.getText().toString())
                                            .setContentIntent(pintent)
                                            .setOngoing(true)
                                            .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM));

                            Toast.makeText(context, eventName.getText().toString(), Toast.LENGTH_SHORT).show();

                            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                            notificationManager.notify(0, mBuilder.build());
                        }
                    };
                    context.registerReceiver( receiver, new IntentFilter(eventName.getText().toString()) );

                    AlarmManager manager = (AlarmManager)(context.getSystemService( Context.ALARM_SERVICE ));

                    manager.set( AlarmManager.RTC, currDate.getTime() + milli, pintent );

                    displayAlarms(context);
                }
            });

            Button delete = new Button(context);
            delete.setText(getString(R.string.deleteBtn));
            delete.setLayoutParams(params);
            delete.setTextColor(Color.BLACK);
            delete.setVisibility(View.VISIBLE);
            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v){
                    cancelAlarm(context, info[0], Integer.parseInt(pair.getKey().toString()));
                    System.out.println(Integer.parseInt(pair.getKey().toString()));
                    displayAlarms(context);
                }
            });

            TextView textView = new TextView(context);
            textView.setTextSize(10);
            textView.setText(info[0] + "\n" + info[1] + " " + info[2]);
            textView.setId(Integer.parseInt(pair.getKey().toString()));
            newLayout.addView(textView);
            newLayout.addView(edit);
            newLayout.addView(delete);

            intentLayout.addView(newLayout);
        }
    }


}